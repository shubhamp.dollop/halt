package com.example.task1;



import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiController
{
    public static String url="https://beta.btimebusiness.com/v2/";
    private static ApiController clintobj;
    private  static Retrofit retrofit;

    public ApiController()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES);

            client.addInterceptor(interceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();


    }

    public static synchronized ApiController getInstance()
    {
        if(clintobj==null)
            clintobj=new ApiController();
        return clintobj;
    }
    public APiset getapi()
    {
        return retrofit.create(APiset.class);
    }



}
