package com.example.task1;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.task1.databinding.ActivityRtrofitByImgBinding;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RtrofitByImg extends AppCompatActivity {

    Uri selectedImageUri;
    String gender="Male";
    int SELECT_PICTURE1 = 100;
    String filePath1="";
    String filePath2="";
    String filePath3="";
    String filePath4="";
    int SELECT_PICTURE2 = 200;
    int SELECT_PICTURE3 = 300;
    int SELECT_PICTURE4 = 400;

    byte [] byteimg1;
    Bitmap bitmap;

    ActivityRtrofitByImgBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding=ActivityRtrofitByImgBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.tvAdharFront.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Dexter.withContext(getApplicationContext())
                        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener()
                        {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse)
                            {
                                imageChooser(SELECT_PICTURE1);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse)
                            {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken)
                            {
                                permissionToken.continuePermissionRequest();
                            }
                        }).check();
            }
        });

        binding.tvAdharBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Dexter.withContext(getApplicationContext())
                        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener()
                        {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse)
                            {
                                imageChooser(SELECT_PICTURE2);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse)
                            {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken)
                            {
                                permissionToken.continuePermissionRequest();
                            }
                        }).check();
            }
        });

        binding.tvGstImg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Dexter.withContext(getApplicationContext())
                        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener()
                        {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse)
                            {
                                imageChooser(SELECT_PICTURE3);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse)
                            {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken)
                            {
                                permissionToken.continuePermissionRequest();
                            }
                        }).check();
            }
        });


        binding.tvPancardImg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Dexter.withContext(getApplicationContext())
                        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener()
                        {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse)
                            {
                                imageChooser(SELECT_PICTURE4);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse)
                            {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken)
                            {
                                permissionToken.continuePermissionRequest();
                            }
                        }).check();
            }
        });

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callapi(binding.etAcNumber.getText().toString(),binding.etIfscCode.getText().toString(),binding.etHolderName.getText().toString(),binding.etPancardNumber.getText().toString(),binding.etAction.getText().toString(),binding.etGstNumeber.getText().toString());
            }
        });

    }

    private void callapi(String acc_number, String ifsc_code,String acc_holder_name,String pancard_no,String action, String gst_number)
    {
    HashMap<String, RequestBody> hashMap = new HashMap<>();
    hashMap.put("acc_number",  RequestBody.create(MediaType.parse("text/plain"), acc_number));
    hashMap.put("ifsc_code",  RequestBody.create(MediaType.parse("text/plain"), ifsc_code));
    hashMap.put("acc_holder_name",  RequestBody.create(MediaType.parse("text/plain"), acc_holder_name));
    hashMap.put("pancard_no",  RequestBody.create(MediaType.parse("text/plain"), pancard_no));
    hashMap.put("action",  RequestBody.create(MediaType.parse("text/plain"), action));
    hashMap.put("gst_number",  RequestBody.create(MediaType.parse("text/plain"), gst_number));
    Log.e("accnu",hashMap+"");


        String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTY0Iiwicm9sZV90eXBlIjoiVXNlciIsInZlcnNpb24iOiIyLjAiLCJBUElfVElNRSI6MTY0Mzc3NzY3OH0.8fTlzXzK6GFvWJPl8VStXQml7zVym363nB2RFRIvH3o";



      MultipartBody.Part part1=prepareimg(binding.ivAdherFront.getDrawable(),"aadharcard_front");
      MultipartBody.Part part2=prepareimg(binding.ivAdherBack.getDrawable(),"aadharcard_back");
      MultipartBody.Part part3=prepareimg(binding.ivGstImg.getDrawable(),"gst_image");
      MultipartBody.Part part4=prepareimg(binding.ivPancardImg.getDrawable(),"pancard_image");
        List<MultipartBody.Part > partList =new ArrayList<>();
        partList.add(part1);
        partList.add(part2);
        partList.add(part3);
        partList.add(part4);
     //   Call<ImageUploadResponseModel> call=ApiController.getInstance().getapi().upload_img(token,hashMap,part1,part2,part3,part4);
        Call<ImageUploadResponseModel> call=ApiController.getInstance().getapi().upload_img(token,hashMap,partList);
        call.enqueue(new Callback<ImageUploadResponseModel>() {
            @Override
            public void onResponse(Call<ImageUploadResponseModel> call, Response<ImageUploadResponseModel> response)
            {
               if (response.isSuccessful())
               {
                   Log.e("Success ",response.message());
               }
            }

            @Override
            public void onFailure(Call<ImageUploadResponseModel> call, Throwable t)
            {
                Log.e("check img",t.toString());
            }
        });
    }

    void imageChooser(int SELECT_PICTURE)// image picker
    {
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)// image picker results
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == SELECT_PICTURE1)
            {
                selectedImageUri = data.getData();
                if (null != selectedImageUri)
                {
                    binding.ivAdherFront.setImageURI(selectedImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (requestCode == SELECT_PICTURE2)
            {
                selectedImageUri = data.getData();
                if (null != selectedImageUri)
                {
                    binding.ivAdherBack.setImageURI(selectedImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (requestCode == SELECT_PICTURE3)
            {
                selectedImageUri = data.getData();
                if (null != selectedImageUri)
                {
                    binding.ivGstImg.setImageURI(selectedImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (requestCode == SELECT_PICTURE4)
            {
                selectedImageUri = data.getData();
                if (null != selectedImageUri)
                {
                    binding.ivPancardImg.setImageURI(selectedImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private MultipartBody.Part prepareimg(Drawable img, String partname)
    {


        Bitmap bitmap = ((BitmapDrawable)img).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byteimg1 = stream.toByteArray();
        Log.e("img",byteimg1.length+"");
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),byteimg1);
        return MultipartBody.Part.createFormData(partname,"aadharcard_front.jpeg",requestBody);
    }



}