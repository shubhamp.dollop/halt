package com.example.task1;

import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SuccessFragement#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SuccessFragement extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView name;
    TextView money;
    Button btn;



    public SuccessFragement() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static SuccessFragement newInstance(String param1, String param2) {
        SuccessFragement fragment = new SuccessFragement();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_success_fragement, container, false);
        name=view.findViewById(R.id.set_name);
        money=view.findViewById(R.id.set_money);
        btn=view.findViewById(R.id.back_btn);

        ((MainActivity)requireActivity()).binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,GravityCompat.START);


        Bundle bundle = this.getArguments();

        if (bundle != null)
        {
            name.setText(bundle.getString("name"));
            money.setText(bundle.getString("money"));
            money.append(" Ruppee");
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ((MainActivity)requireActivity()).navController.popBackStack(R.id.nav_home,false);// back to stack
            }
        });
        return  view;
    }
}