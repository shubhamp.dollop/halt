package com.example.task1;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.task1.Utils.UserAccount;

public class Payment_fragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TextView name;
    Button btn;
    EditText money;
    Bundle bundle;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Payment_fragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static Payment_fragment newInstance(String param1, String param2) {
        Payment_fragment fragment = new Payment_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_payment_fragment, container, false);

        bundle = this.getArguments();
        if (bundle != null)
        {
            name.setText(bundle.getString("name"));
        }

        name=view.findViewById(R.id.settext);
        btn=view.findViewById(R.id.add_btn);
        money=view.findViewById(R.id.et_money);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                validation();
            }
        });

        return view;
    }

    private void validation()
    {
        if (UserAccount.isValidAmount(requireActivity(),money))
        {
             bundle = this.getArguments();

            assert bundle != null;
            bundle.putString("money",money.getText().toString());
            ((MainActivity)requireActivity()).navController.navigate(R.id.successFragement,bundle);
        }else
        {
            UserAccount.EditTextPointer.setError(UserAccount.errorMessage);
            UserAccount.EditTextPointer.requestFocus();
        }
    }
}