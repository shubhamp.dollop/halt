package com.example.task1;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.task1.Utils.UserAccount;


public class FormFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    EditText name,mail,password;
    Button btn;

    public FormFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FormFragment newInstance(String param1, String param2) {
        FormFragment fragment = new FormFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)

    {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_form, container, false);
        name=view.findViewById(R.id.et_name);
        mail=view.findViewById(R.id.et_mail);
        password=view.findViewById(R.id.et_password);
        btn=view.findViewById(R.id.btn_submit);

        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                validation();
            }
        });

        return view;
    }

    private void validation()
    {
        if (UserAccount.isEmpty(requireActivity(),name))
        {
            if (UserAccount.isEmailValid(requireActivity(),mail))
            {
                if (UserAccount.isEmpty(requireActivity(),password))
                {

                    Bundle data= new Bundle();
                    data.putString("name",name.getText().toString());
                    data.putString("mail",mail.getText().toString());
                    data.putString("password",password.getText().toString());
                    ((MainActivity)requireActivity()).navController.navigate(R.id.payment_fragment,data);

                }else
                {
                    UserAccount.EditTextPointer.setError(UserAccount.errorMessage);
                    UserAccount.EditTextPointer.requestFocus();
                }
            }else
            {
                UserAccount.EditTextPointer.setError(UserAccount.errorMessage);
                UserAccount.EditTextPointer.requestFocus();
            }
        }else
        {
            UserAccount.EditTextPointer.setError(UserAccount.errorMessage);
            UserAccount.EditTextPointer.requestFocus();
        }


    }
}