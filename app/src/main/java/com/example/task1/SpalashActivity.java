package com.example.task1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.task1.Utils.Utils;

public class SpalashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalash);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                Utils.I(SpalashActivity.this,MainActivity.class,null);
            }
        },4000);
    }
}