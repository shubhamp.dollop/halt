package com.example.task1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import com.example.task1.databinding.ActivityMapExmActvityBinding;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapExmActvity extends AppCompatActivity {
    SupportMapFragment smf;
    FusedLocationProviderClient client;
    ActivityMapExmActvityBinding binding;
    Geocoder geocoder;
    GoogleMap gMap;
//    MarkerOptions markerOptions1;
    List<Marker> markerList = new ArrayList<>();

    List<LatLng> latLngList = new ArrayList<>();
//    Marker marker;
    List<Address> addresses;
    String address;
    Polyline currentpolyline;
    double latitude; //send to put
    double longitude;// send to put
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {


    super.onCreate(savedInstanceState);
        binding=ActivityMapExmActvityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        smf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fg_map_id);
        client = LocationServices.getFusedLocationProviderClient(this);
        Dexter.withContext(getApplicationContext())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse)
                    {
                        getmyloction();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        try {
                            Uri uri = Uri.fromParts("package", getPackageName(), "");
                            intent.setData(uri);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken)
                    {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();

        binding.drawTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if (currentpolyline!=null)
                 {
                     currentpolyline.remove();
                 }
                 else
                 {
                     PolylineOptions polylineOptions = new PolylineOptions()
                             .addAll(latLngList).clickable(true);
                     currentpolyline=gMap.addPolyline(polylineOptions);
                 }
            }
        });

//        binding.btConfirmLocationId.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                Intent intent =  new Intent();
//                intent.putExtra("latitude",latitude);
//                intent.putExtra("longitude",longitude);
//                intent.putExtra("address",address);
//
//                Bundle bundle = new Bundle();
//                bundle.putString("address", address);
//                HomeFragment fragobj = new HomeFragment();
//                fragobj.setArguments(bundle);
//                setResult(RESULT_OK,intent);
//                finish();
//
//            }
//        });

    }
    public void getmyloction()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>()
        {
            @Override
            public void onSuccess(Location location)
            {
                smf.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap)
                    {
                        gMap=googleMap;
//                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                        latitude=location.getLatitude();//send to put
//                        longitude=location.getLongitude();// send to put
//
//                        markerOptions1 = new MarkerOptions().position(latLng).title("Your Location");
//                        marker=googleMap.addMarker(markerOptions1);
//                        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15 ));
//                        gMap.addMarker(markerOptions1);
//
//                        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//
//                        try {
//                            addresses = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
//                            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
////                            binding.tvAddressAreaId.setText(address);
//                        } catch (IOException e) {
//                            e.printStackTrace();555555
//                        }
                        gMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
                        {
                            @Override
                            public void onMapClick(LatLng latLng)
                            {
//                                binding.tvAddressAreaId.setText("");

                                MarkerOptions markerOptions1 = new MarkerOptions().position(latLng);
                                Marker marker= gMap.addMarker(markerOptions1);
//                                gMap.clear();
//                                marker.remove();
//                                markerOptions1 = new MarkerOptions().position(latLng).title("Your Location");
//                                marker=gMap.addMarker(markerOptions1);
//                                gMap.addMarker(markerOptions1);

                                latLngList.add(latLng);
                                markerList.add(marker);

//                                try {
//                                    addresses=geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
//                                    address = addresses.get(0).getAddressLine(0);
////                                    binding.tvAddressAreaId.setText(address);
//                                    latitude=latLng.latitude;
//                                    longitude=latLng.longitude;
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
                             }
                        });
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                        {
                            return;
                        }


                    }
                });

            }
        });
    }





}
