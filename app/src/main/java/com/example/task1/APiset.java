package com.example.task1;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface APiset
{
    @Multipart
    @POST("update_business_profile")
    Call<ImageUploadResponseModel> upload_img(
            @Header("Authorization")String token,
            @PartMap HashMap<String, RequestBody> data,
//            @Part MultipartBody.Part part1,
//            @Part MultipartBody.Part part2,
//            @Part MultipartBody.Part part3,
            @Part List<MultipartBody.Part > part4
    );

}
